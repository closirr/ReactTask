import axios from 'axios';
import { from } from 'rxjs';
import { generateGuid } from '../utils/guid-generator.service';

export class NotesApi {

    getNotes() {
        return from(axios.get('http://localhost:3000/notes'));
    }

    createNote(noteText) {
        return from(axios.post('http://localhost:3000/notes', { id: generateGuid(), text: noteText, done: false, isArchived: false }));
    }
    deleteNote(note) {
        return from(axios.delete('http://localhost:3000/notes/' + note.id));
    }

    editNode(note) {
        return from(axios.put('http://localhost:3000/notes/' + note.id, note));
    }
}