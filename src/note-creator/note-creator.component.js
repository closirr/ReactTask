import React, { Component } from 'react';
import logo from '../logo.svg';
import '../App.css';
import { NotesApi } from '../api/notes.api';
import { ADD_NOTES } from '../redux/actionTypes';
import { connect } from "react-redux";



class NoteCreator extends Component {
  constructor(props) {
    super(props);
    this.state = { noteText: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ noteText: event.target.value });
  }

  handleSubmit() {
    this.props.addNotes(this.state.noteText);
  }

  render() {
    return (
      <div>
        <label>
          Create Note
          <input type="text" value={this.state.noteText} onChange={this.handleChange} />
        </label>
        <button  style={{width: '50px', height: '21px'}} onClick={this.handleSubmit}>submit</button>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addNotes: (notes) => (new NotesApi()).createNote(notes).subscribe((note) => dispatch(addNotesAction(note.data)))
  };
};

const addNotesAction = (notes) => { return { type: ADD_NOTES, payload: notes } };

const noteContainer = connect(null, mapDispatchToProps)(NoteCreator);


export default noteContainer;
