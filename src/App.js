import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import NoteCreator from './note-creator/note-creator.component';
import NotesContainer from './notes-container/notes-container.component';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { connect } from 'react-redux';
import { NotesApi } from './api/notes.api';
import { addNotesAction } from './redux/actionCreators';
import NoteAchive from './note-archive/note-archive.component';

class App extends Component {

  constructor(props) {
    super(props);

    this.props.initNotes();
  }

  render() {
    return (
      <Router>


        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <Link to="/archive"><h1 >Archive</h1></Link>
          </header>
          <p></p>
          <NoteCreator />
          <div>
            <Route exact path="/" component={NotesContainer} />
            <Route path="/archive" component={NoteAchive} />
          </div>
        </div>
      </Router>

    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    initNotes: () => (new NotesApi()).getNotes().subscribe(n => n.data.map((n) => dispatch(addNotesAction(n))))
  };
};

export default connect(null, mapDispatchToProps)(App);
