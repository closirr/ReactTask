import React, { Component } from 'react';
import { NotesApi } from '../api/notes.api';
import NoteCard from './note-card/note-card.component';
import store from '../redux/store'
import { ADD_NOTES } from '../redux/actionTypes';
import { connect } from "react-redux";
import { addNotesAction } from '../redux/actionCreators';

class NotesContainer extends Component {
    constructor(props) {
        super(props);
        this.state = { searchText: "" };
    }

    onSearchTextChange(e) {
        this.setState({ searchText: e.target.value });
    }

    isNoteShow(note) {
        return note.text.includes(this.state.searchText) && this.props.isOnlyArchived ? note.isArchived: !note.isArchived;
    }

    render() {
        return (
            <div>
               <p> search <input title='search' defaultValue="search" value={this.state.searchText} onChange={(e) => this.onSearchTextChange(e)}></input></p>
                {this.props.notes.map((note, i) => this.isNoteShow(note) ? < NoteCard note={note} key={i} /> : null)}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { notes: state.notes };
};

const notesContainer = connect(mapStateToProps)(NotesContainer);

export default notesContainer;
