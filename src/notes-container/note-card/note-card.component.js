import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DELETE_NOTE } from '../../redux/actionTypes';
import { NotesApi } from '../../api/notes.api';
import { deleteNoteAction, toogleDoneAction, editNoteAction } from '../../redux/actionCreators';
import './note-card.css';

class NoteCard extends Component {
  constructor(props) {
    super(props);
    this.state = { isEdit: false, noteText: this.props.note.text }
  }

  removeNote() {
    this.props.deleteNote(this.props.note);
  }

  onNoteChange(e) {
    this.setState({ noteText: e.target.value });
  }

  toggleEdit() {
    this.setState({ isEdit: true });
  }

  handleSubmit() {
    this.props.editNote({ ...this.props.note, text: this.state.noteText });
    this.setState({ isEdit: false });
  }

  render() {

    return (
      <div>
        {this.state.isEdit ?
          <p>
            <input title='' value={this.state.noteText} onChange={(e) => this.onNoteChange(e)}></input>
            <button style={{ width: '50px', height: '21px' }} onClick={() => this.handleSubmit()}>submit</button>
          </p> :
          <p>{this.props.note.text}</p>}
        <button onClick={() => this.props.deleteNote(this.props.note)}>remove</button>
        <button className={this.props.note.done ? 'done' : null} onClick={() => this.props.toogleDone(this.props.note)}>mark as done</button>
        {!this.state.isEdit ? <button onClick={() => this.toggleEdit()}> edit</button> : null}
        <button onClick={() => this.props.editNote({...this.props.note, isArchived: !this.props.note.isArchived})}>{this.props.note.isArchived ? 'unarchive': 'archive'}</button>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const api = new NotesApi();
  return {
    deleteNote: (note) => api.deleteNote(note).subscribe(n => dispatch(deleteNoteAction(note.id))),
    toogleDone: (note) => api.editNode(note).subscribe(n => dispatch(toogleDoneAction(note.id))),
    editNote: (note) => api.editNode(note).subscribe(n => dispatch(editNoteAction(note)))
  };
};

const noteCard = connect(null, mapDispatchToProps)(NoteCard);

export default noteCard;
