export const DELETE_NOTE = 'DELETE_NOTE';
export const ADD_NOTES = 'ADD_NOTES';
export const GET_NOTES = 'GET_NOTES';
export const TOOGLE_DONE = 'TOOGLE_DONE';
export const EDIT_NOTE = 'EDIT_NOTE';
