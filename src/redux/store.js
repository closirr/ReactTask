import {
  ADD_NOTES,
  EDIT_NOTE,
  DELETE_NOTE,
  GET_NOTES,
  TOOGLE_DONE
} from './actionTypes';
import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const notesReducer = function (state = { notes: [] }, action) {
  switch (action.type) {
    case ADD_NOTES:
      return {
        ...state,
        notes: [...state.notes, action.payload]
      };
    case GET_NOTES:
      return {
        ...state,
        notes: [...state.notes, ...action.payload]
      };
    case DELETE_NOTE:
      return {
        ...state,
        notes: state.notes.filter(x => x.id !== action.payload)
      }
    case TOOGLE_DONE:
      return {
        ...state,
        notes: state.notes.map(n => n.id === action.payload ? { ...n, done: !n.done } : n)
      }
    case EDIT_NOTE:
      return {
        ...state,
        notes: state.notes.map(n => n.id === action.payload.id ? { ...n, text: action.payload.text } : n)
      }
    default:
      return state;
  }
}

const store = createStore(notesReducer, compose(applyMiddleware(thunk)));

export default store;
