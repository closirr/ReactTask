import { ADD_NOTES, DELETE_NOTE, TOOGLE_DONE, EDIT_NOTE } from './actionTypes';
export const addNotesAction = (note) => { return { type: ADD_NOTES, payload: note } };
export const deleteNoteAction = (id) => { return { type: DELETE_NOTE, payload: id } }
export const toogleDoneAction = (note) => { return { type: TOOGLE_DONE, payload: note } }
export const editNoteAction = (note) => { return { type: EDIT_NOTE, payload: note } }
